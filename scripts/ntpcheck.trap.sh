#!/bin/bash

offset=$(/usr/sbin/ntpq -pn | /usr/bin/awk 'BEGIN { offset=1000 } $1 ~ /\*/ { offset=$9 } END { print offset }')
stratum=$(/usr/sbin/ntpq -pn | /usr/bin/awk 'BEGIN { stratum=99 } $1 ~ /\*/ { stratum=$3 } END { print stratum }')
peers=$(ntpq -pn | grep -E -c '^\*|^\+')

echo "- ntp.offset $offset"
echo "- ntp.stratum $stratum"
echo "- ntp.peers $peers"

