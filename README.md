Documention for Zabbix Scripts

Work-in-Progress

The agents provided in this bundle are intended to be used for Zabbix Trapper
agents, run on the server and batched together and sent as bulk. Documentation
covering how these work will be provided further in this documentation.

Suggestion installation path is to place the scripts/ directory into:
	/etc/zabbix/scripts/

And then create a directory:
	/etc/zabbix/trap.d/

If you need more granularity, you can make a trap.d/daily, trap.d/hourly, etc
as needed and adjust your crons to run the runtrap with the path accordingly.

Inside your trap.d path should be either symlinks directly back to the
scripts/trappername or a script that runs the trapper agent with neccessary
arguments as needed. You can number the links/scripts in the trap.d so they
execute in a specific order.

One thing to note, it's best to make sure everything in your trap.d is uniquely
named. The runtrap script is designed to prevent stalling trappers by killing
them after a number of execution attempts (default of 3). Also, when executing
runtrap from cron, it's best to always insure it's run by the full pathname so
it can kill itself accordingly when deadlocks are detected. This will allow
the next run to attempt running the trappers again.

