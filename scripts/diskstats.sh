#!/bin/bash

mode=${1-trap}
mountpath=${2-all}
stat=${3,,}

mode=${mode,,}

#echo "mode: $mode"
#echo "path: $mountpath"
#echo "stat: $stat"

if [[ "$mode" = "active" && ! -d "$mountpath" ]];
then
	echo "ZBX_NOTSUPPORTED"
	exit 1
fi

if [[ "$mode" = "help" ]];
then
	case $mountpath in
	zabbix)	myself=$(readlink -f $0)
		echo "UserParameter vfs.read.ops[*],$myself active \$1"
		echo "UserParameter vfs.read.ms[*],$myself active \$1"
		echo "UserParameter vfs.read.sector[*],$myself active \$1"
		echo "UserParameter vfs.write.ops[*],$myself active \$1"
		echo "UserParameter vfs.write.ms[*],$myself active \$1"
		echo "UserParameter vfs.write.sector[*],$myself active \$1"
		echo "UserParameter vfs.io.active[*],$myself active \$1"
		echo "UserParameter vfs.io.ms[*],$myself active \$1"
		;;
	*)	echo "COMMAND:"
		echo "  ${0##*/} active path {STAT}"
		echo "  ${0##*/} trap [path]"
		echo "  ${0##*/} help [zabbix]"
		echo " "
		echo "STAT:"
		echo "  readops      - Read Operations"
		echo "  readms       - Read time in Miliseconds"
		echo "  readsectors  - Read sectors"
		echo "  writeops     - Write operations"
		echo "  writems      - Write time in Miliseconds"
		echo "  writesectors - Write sectors"
		echo "  ioactive     - Active IO threads"
		echo "  ioms         - IO time in Miliseconds"
	esac
	exit 0
fi

#exit 0

while read -r spec file vfstype mntops freq passno
do
	if [[ $spec = /dev* ]]
	then
		if [[ "$mountpath" != "all" && "$mountpath" != "$file" ]];
		then
			continue
		fi
		device=$(readlink -f $spec)
		device=${device##*/}
		read -r no no no readops no readsectors readms writeops no writesectors writems ioactive ioms no <<< $(grep $device /proc/diskstats | head -1)

		case $mode in
		trap)
			echo "- vfs.read.ops[$file] $readops"
			echo "- vfs.read.ms[$file] $readms"
			echo "- vfs.read.sectors[$file] $readsectors"
			echo "- vfs.write.ops[$file] $writeops"
			echo "- vfs.write.ms[$file] $writems"
			echo "- vfs.write.sectors[$file] $writesectors"
			echo "- vfs.io.active[$file] $ioactive"
			echo "- vfs.io.ms[$file] $ioms"
			;;
		active)
			case $stat in
			readops)	echo "$readops";;
			readms)		echo "$readms";;
			readsectors)	echo "$readsectors";;
			writeops)	echo "$writeops";;
			writems)	echo "$writems";;
			writesectors)	echo "$writesectors";;
			ioactive)	echo "$ioactive";;
			ioms)		echo "$ioms";;
			*)		echo "ZBX_NOTSUPPORTED";;
			esac
		esac
	fi
done < /proc/mounts

